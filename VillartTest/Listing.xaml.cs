﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for Listing.xaml
    /// </summary>
    public partial class Listing : UserControl
    {
        public Boolean b = new Boolean();
        public Listing()
        {
            InitializeComponent();
        }

        public delegate void myListItem(object sender, EventArgs e);
        public event myListItem myListItemHandler;
 
        private void btnItem_TouchDown(object sender, TouchEventArgs e)
        {
            myListItemHandler(sender, null);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            
        }
        private void ScrollViewer_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
