﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for Album.xaml
    /// </summary>
    public partial class Album : UserControl
    {
        public Album()
        {
            InitializeComponent();
        }
        
       
        public List<ImageDetails> lstImgDet;
        private bool isFromNextClick = false;
        private bool isFromPrevClick = false;
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            lstImgDet = ZoomCartAlbumStore.LstAlbumImgDetails.ToList();

            if (lstImgDet.Count ==1)
            {
                //viewportLeft.DataContext = lstImgDet[0].Path;
                //viewportLeftFlipUp.DataContext = lstImgDet[0].Path;
                viewportRightFlipUp.DataContext = lstImgDet[0].Path;
                viewportRight.DataContext = lstImgDet[0].Path;
            }
            else if (lstImgDet.Count > 1)
            {
                viewportLeft.DataContext = lstImgDet[0].Path;
                viewportLeftFlipUp.DataContext = lstImgDet[0].Path;
                viewportRightFlipUp.DataContext = lstImgDet[1].Path;
                viewportRight.DataContext = lstImgDet[1].Path;
            }

        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            isFromNextClick = true;
            string lastFetchedImg = viewportRight.DataContext as string;
            int index = lstImgDet.FindIndex(c => c.Path == lastFetchedImg);

            if (index == -1 && isFromPrevClick == true)
                index = 1;
            isFromPrevClick = false;

            if (!string.IsNullOrEmpty(lastFetchedImg) && index >= 0 && lstImgDet.Count >= (index + 2))
            {
                viewportLeft.DataContext = lstImgDet[index - 1].Path;
                viewportRightFlipUp.DataContext = lstImgDet[index].Path;
                viewportLeftFlipUp.DataContext = lstImgDet[index + 1].Path;
                if (lstImgDet.Count > (index + 2))
                    viewportRight.DataContext = lstImgDet[index + 2].Path;
                else
                    viewportRight.DataContext = null;


                Storyboard nxtSB = (Storyboard)TryFindResource("RTL_Flip_SB");
                nxtSB.Begin();

            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            isFromPrevClick = true;
            string lastFetchedImg = viewportRight.DataContext as string;
            int index = lstImgDet.FindIndex(c => c.Path == lastFetchedImg);
            if (index == -1 && isFromNextClick == true)
                index = lstImgDet.Count;
            isFromNextClick = false;
            if (index >= 0 && (index - 3) >= 0)
            {
                viewportLeft.DataContext = lstImgDet[index - 3].Path;
                viewportRightFlipUp.DataContext = lstImgDet[index - 2].Path;
                viewportLeftFlipUp.DataContext = lstImgDet[index - 1].Path;
                if ((index - 3) >= 0)
                    viewportRight.DataContext = lstImgDet[index - 2].Path;
                else
                    viewportRight.DataContext = null;

                Storyboard prevSB = (Storyboard)TryFindResource("LTR_Flip_SB");
                prevSB.Begin();
            }
        }


        private void btnCover_Click(object sender, RoutedEventArgs e)
        {
            Storyboard CoverSBStart = (Storyboard)TryFindResource("AlbumCover_SB");
            CoverSBStart.Begin();

        }

    }
}
