﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for Close.xaml
    /// </summary>
    public partial class Close : UserControl
    {
        public ImageDetails samp;
        public event EventHandler myCloseEvent;
        public event EventHandler myCartEvent;
        public Close()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            List<ImageDetails> lstImgDetails = new List<ImageDetails>();
            lstImgDetails.Add(samp);

            this.DataContext = lstImgDetails.ToList();
            Panel.SetZIndex(imgCompare, 9999);


        }

        public void btnDelete_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCloseEvent(this, null);
        }

        public void btnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCartEvent(this, null);
        }
    }
}
