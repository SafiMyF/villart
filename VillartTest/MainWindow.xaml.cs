﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.IO;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Boolean b = new Boolean();
        public ZoomCart zc;
        public List<Image> lstimg;
        public ImageDetails samp;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            

            //Video Code Start

            //MediaTimeline timeline = new MediaTimeline(new Uri("play1.mp3", UriKind.Relative));
            //timeline.RepeatBehavior = RepeatBehavior.Forever;
            //MediaClock clock = timeline.CreateClock();
            //MediaPlayer player = new MediaPlayer();
            //player.Clock = clock;
            //VideoDrawing drawing = new VideoDrawing();
            ////drawing.Rect = new Rect(0, 0, 300, 200);
            ////drawing.Player = player;
            //DrawingBrush brush = new DrawingBrush(drawing);
            //this.Background =  brush;
            //Video Code End


            this.ManipulationStarting -= MainWindow_ManipulationStarting;
            this.ManipulationDelta -= MainWindow_ManipulationDelta;
            this.ManipulationStarting+=MainWindow_ManipulationStarting;
            this.ManipulationDelta+=MainWindow_ManipulationDelta;
        }

        ManipulationModes currentMode = ManipulationModes.All;
        private void MainWindow_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = this;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;
            base.OnManipulationStarting(args);
        }

        private void MainWindow_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            MatrixTransform xform = element.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;
            ManipulationDelta delta = args.DeltaManipulation;
            Point center = args.ManipulationOrigin;
            matrix.Translate(-center.X, -center.Y);
            //matrix.Scale(delta.Scale.X, delta.Scale.Y);
            matrix.Translate(center.X, center.Y);
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            xform.Matrix = matrix;
            args.Handled = true;
            base.OnManipulationDelta(args);
        }


        ItemView iv;
        Listing lst;
        Category ctg;

        private void Grid_TouchDown(object sender, TouchEventArgs e)
        {
            //ContentPanel.Content = null;
            //Storyboard sb = (Storyboard)TryFindResource("AlbumSB");
            //sb.Stop();

            //ctg = new Category();
            //ContentPanel.Content = ctg;
            //ctg.myCategListHandler += ctg_myCategListHandler;
        }

        private void button2_Click_1(object sender, RoutedEventArgs e)
        {
            ContentPanel.Content = null;
            Storyboard sb = (Storyboard)TryFindResource("AlbumSB");
            sb.Stop();

            ctg = new Category();
            ContentPanel.Content = ctg;
            ctg.myCategListHandler += ctg_myCategListHandler;

            //ctg.myCategPortrait += ctg_myCategPortrait;
        }

        private void button2_TouchDown_1(object sender, TouchEventArgs e)
        {
            ContentPanel.Content = null;
            Storyboard sb = (Storyboard)TryFindResource("AlbumSB");
            sb.Stop();

            ctg = new Category();
            ContentPanel.Content = ctg;
            ctg.myCategListHandler += ctg_myCategListHandler;

            ctg.myCategListHandlerPortrait += ctg_myCategListHandlerPortrait;
            ctg.myCategListHandlerFashions += ctg_myCategListHandlerFashions;
            ctg.myCategListHandlerKids += ctg_myCategListHandlerKids;
            ctg.myCategListHandlerCorporate += ctg_myCategListHandlerCorporate;
            ctg.myCategListHandlerOthers += ctg_myCategListHandlerOthers;
            //ctg.myCategPortrait += ctg_myCategPortrait;
        }

        void ctg_myCategListHandlerOthers(object sender, EventArgs e)
        {
            startLetter = "o";
            ListBinding(startLetter);
        }

        void ctg_myCategListHandlerCorporate(object sender, EventArgs e)
        {
            startLetter = "c";
            ListBinding(startLetter);
        }

        void ctg_myCategListHandlerKids(object sender, EventArgs e)
        {
            startLetter = "k";
            ListBinding(startLetter);
        }

        void ctg_myCategListHandlerFashions(object sender, EventArgs e)
        {
            startLetter = "f";
            ListBinding(startLetter);
        }

        void ctg_myCategListHandlerPortrait(object sender, EventArgs e)
        {
            startLetter = "p";
            ListBinding(startLetter);
            
        }

        

        void ctg_myCategListHandler(object sender, EventArgs e)
        {
            startLetter = "w";
            ListBinding(startLetter);
        }

        private void lst_myListItemHandler(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            samp = btn.DataContext as ImageDetails;
            if (!checkChildren(samp))
            {
                iv = new ItemView();

                iv.btn = btn;
                iv.IsManipulationEnabled = true;

                //Style ivStyle =  (Style)TryFindResource("ItemViewSlide");
                //iv.ivMainGrid.Style = ivStyle;
                
                iv.RenderTransform = new MatrixTransform(1, 0, 0, 1, 10, 10);
                
                MainContent.Children.Add(iv);
                iv.BringIntoView();
                Panel.SetZIndex(iv, 9999);
                iv.myZoomCartHandler += iv_myZoomCartHandler;
                iv.myZoomAlbumHandler += iv_myZoomAlbumHandler;
            }
        }

        private bool checkChildren(ImageDetails samp)
        {
            foreach (Control ctrl in MainContent.Children)
            {
                if (ctrl is ItemView)
                {
                    ItemView iv = ctrl as ItemView;

                    List<ImageDetails> lstSample = iv.stkPnlBrf.DataContext as List<ImageDetails>;
                    if (lstSample.Contains(samp))
                    {
                        iv.Visibility = Visibility.Visible;
                        iv.BringIntoView();
                        return true;
                    }
                }
            }
            return false;
        }


        void iv_myZoomAlbumHandler(object sender, EventArgs e)
        {
            txtZoomAlbumCnt.Text = ZoomCartAlbumStore.AlbumCnt.ToString();
        }

        public void iv_myZoomCartHandler(object sender, EventArgs e)
        {
            txtZoomCartCount.Text = ZoomCartAlbumStore.CartCnt.ToString();
        }

        
       
        private void btnZoomCart_TouchDown(object sender, TouchEventArgs e)
        {

            MainContent.Children.Clear();
            zc = new ZoomCart();
            zc.myEventDecreaseCartCnt += zc_myEventDecreaseCartCnt;
            zc.myEventDecreaseCartIncreaseAlbumCntUpdate += zc_myEventDecreaseCartIncreaseAlbumCntUpdate;
            zc.lstImgDetails = ZoomCartAlbumStore.LstZoomCartImgDetails.ToList();
            ContentPanel.Content = zc;
            
        }

        void zc_myEventDecreaseCartIncreaseAlbumCntUpdate(object sender, EventArgs e)
        {
            txtZoomCartCount.Text = Convert.ToString(ZoomCartAlbumStore.CartCnt);
            txtZoomAlbumCnt.Text = Convert.ToString(ZoomCartAlbumStore.AlbumCnt);
        }

        void zc_myEventDecreaseCartCnt(object sender, EventArgs e)
        {
            txtZoomCartCount.Text = Convert.ToString(ZoomCartAlbumStore.CartCnt);
            txtZoomAlbumCnt.Text = Convert.ToString(ZoomCartAlbumStore.AlbumCnt);
        }

        Album alb;
        private void btnAlbum_Click(object sender, RoutedEventArgs e)
        {
            MainContent.Children.Clear();
            alb = new Album();
            ContentPanel.Content = alb;
            
        }        

        private void btnAlbum_TouchDown(object sender, TouchEventArgs e)
        {
            MainContent.Children.Clear();
            alb = new Album();
            
            ContentPanel.Content = alb;
        }

        
        private void btnHome_TouchDown(object sender, TouchEventArgs e)
        {
            CartAlbumPanel.Visibility = Visibility.Hidden;
            CategButtonPanel.Visibility = Visibility.Hidden;
            MainContent.Children.Clear();

            Storyboard sb = (Storyboard)TryFindResource("AlbumSB");
            sb.Begin();
            ContentPanel.Content = HomePlay;

            txtZoomAlbumCnt.Text = "0";
            txtZoomCartCount.Text = "0";
            
            ZoomCartAlbumStore.LstAlbumImgDetails.Clear();
            ZoomCartAlbumStore.LstZoomCartImgDetails.Clear();
            ZoomCartAlbumStore.AlbumCnt = 0;
            ZoomCartAlbumStore.CartCnt = 0;
            
        }

        private void btnLeftWedding_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "w";
            ListBinding(startLetter);
            
        }


        public string startLetter;

        private void btnLeftPortrait_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "p";
            ListBinding(startLetter);
            
        }

        private void btnLeftFashions_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "f";
            ListBinding(startLetter);
        }

        private void btnLeftKids_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "k";
            ListBinding(startLetter);
        }

        private void btnLeftCorporate_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "c";
            ListBinding(startLetter);
        }

        private void btnLeftOthers_TouchDown_1(object sender, TouchEventArgs e)
        {
            startLetter = "o";
            ListBinding(startLetter);
        }

        //public string startAlpha;

        public void ListBinding(string startAlpha)
        {
            MainContent.Children.Clear();
            lst = new Listing();
            ContentPanel.Content = lst;
            CartAlbumPanel.Visibility = Visibility.Visible;
            CategButtonPanel.Visibility = Visibility.Visible;
            lst.myListItemHandler += lst_myListItemHandler;

            string root = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            string[] supportedExtensions = new[] { ".bmp", ".jpeg", ".jpg", ".png", ".tiff" };
            var files = Directory.GetFiles(System.IO.Path.Combine(root, "ListingImages"), "*.*").Where(s => supportedExtensions.Contains(System.IO.Path.GetExtension(s).ToLower()));
            //var files = Directory.GetFiles(Environment.CurrentDirectory + @"\ListingImages");

            List<ImageDetails> images = new List<ImageDetails>();
            foreach (var file in files)
            {
                ImageDetails id = new ImageDetails()
                {
                    Path = file,
                    FileName = System.IO.Path.GetFileName(file),

                    Extension = System.IO.Path.GetExtension(file)
                };
                //string[] strGet = file.Split('\\');
                string strStart = file.Split('\\').Last(); //strGet[7].ToString();
                b = strStart.StartsWith(startAlpha);
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.UriSource = new Uri(file, UriKind.Absolute);
                img.EndInit();
                id.Width = img.PixelWidth;
                id.Height = img.PixelHeight;

                // I couldn't find file size in BitmapImage
                FileInfo fi = new FileInfo(file);
                id.Size = fi.Length;
                if (b == true)
                {
                    images.Add(id);
                }
            }

            lst.ImageList.ItemsSource = images;
        }

     
    }
}
