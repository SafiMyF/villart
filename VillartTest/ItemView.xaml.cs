﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for ItemView.xaml
    /// </summary>
    public partial class ItemView : UserControl
    {
        public Button btn;
        public Image img;
        public Image img1;
        public string samp;
        public delegate void myZoomCart(object sender, EventArgs e);
        
        public event EventHandler myZoomAlbumHandler;
        public event EventHandler myZoomCartHandler;

        public ItemView()
        {
            InitializeComponent();
        }

        private void btnCloseIv_Click(object sender, RoutedEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }

        private void btnCloseIv_TouchDown(object sender, TouchEventArgs e)
        {
            this.Visibility = Visibility.Collapsed;
        }

        public ImageDetails imgDetails;
        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (btn != null)
            {
                imgDetails = btn.DataContext as ImageDetails;
                //string strReplace = imgDetails.Path.Replace("ListingImages", "ZoomCartImages");
                //imgDetails.Path = strReplace;
                //List<ImageDetails> lstImgDetails = new List<ImageDetails>();
                //lstImgDetails.Add(imgDetails);

                //grdImg.DataContext = lstImgDetails.ToList();
                //stkPnlBrf.DataContext = lstImgDetails.ToList();

                grdImg.Children.Clear();
                BitmapImage bip = new BitmapImage();
                img = new Image();
                img1 = btn.Content as Image;
                string strImg = Convert.ToString(img1.Source);
                string strFirst = strImg.Split('/').Last();
                //string strReplace = strImg.Replace("ListingImages", "ZoomCartImages");
                string strReplace = imgDetails.Path.Replace("ListingImages", "ZoomCartImages");

                imgDetails.Path = strReplace;
                List<ImageDetails> lstImgDetails = new List<ImageDetails>();
                lstImgDetails.Add(imgDetails);
                grdImg.DataContext = lstImgDetails.ToList();
                

                ImageSourceConverter imgConv = new ImageSourceConverter();
                ImageSource imageSource = (ImageSource)imgConv.ConvertFromString(strReplace);
                img.Source = imageSource;
                grdImg.Children.Add(img);

            }
        }

        //private void btnCart_TouchDown_1(object sender, TouchEventArgs e)
        //{
        //    ZoomCartAlbumStore.LstZoomCartImgDetails.Add(imgDetails);
        //    ZoomCartAlbumStore.CartCnt += 1;
        //    myZoomCartHandler(this, null);
        //    this.Visibility = Visibility.Collapsed;
        //}

        //private void btnAlbum_TouchDown_1(object sender, TouchEventArgs e)
        //{
        //    ZoomCartAlbumStore.LstAlbumImgDetails.Add(imgDetails);
        //    ZoomCartAlbumStore.AlbumCnt += 1;
        //    myZoomAlbumHandler(this, null);
        //    this.Visibility = Visibility.Collapsed;
        //}

        private void btnCart_TouchDown_1(object sender, TouchEventArgs e)
        {
            bool isSuccess = false;

            List<ImageDetails> samp = stkPnlBrf.DataContext as List<ImageDetails>;

            foreach (ImageDetails lstSampl in samp)
            {
                List<ImageDetails> lstprod = ZoomCartAlbumStore.LstZoomCartImgDetails.Where(c => c.FileName.Equals(lstSampl.FileName)).ToList();
                //List<ImageDetails> lstprod = ZoomCartAlbumStore.LstZoomCartImgDetails.Where(c => c.categoryId.Equals(lstSampl.Name .categoryId) && c.id.Equals(lstSampl.id)).ToList();
                if (lstprod.Count == 0)
                {
                    isSuccess = true;
                    ZoomCartAlbumStore.LstZoomCartImgDetails.Add(imgDetails);
                    ZoomCartAlbumStore.CartCnt += 1;
                    myZoomCartHandler(this, null);
                }
                else
                {
                    MessageBox.Show("Already added the selected item to zoom cart. Please choose another one....", "Villart Photography", MessageBoxButton.OK, MessageBoxImage.Information);

                    this.Visibility = Visibility.Hidden;
                }
            }
            if (isSuccess)
            {
                //this.Close();
                System.Threading.Thread.Sleep(500);
                this.Visibility = Visibility.Hidden;
            }
        }

        private void btnAlbum_TouchDown_1(object sender, TouchEventArgs e)
        {
            bool isSuccess = false;

            List<ImageDetails> samp = stkPnlBrf.DataContext as List<ImageDetails>;

            foreach (ImageDetails lstSampl in samp)
            {
                List<ImageDetails> lstprod = ZoomCartAlbumStore.LstAlbumImgDetails.Where(c => c.FileName.Equals(lstSampl.FileName)).ToList();
                //List<ImageDetails> lstprod = ZoomCartAlbumStore.LstZoomCartImgDetails.Where(c => c.categoryId.Equals(lstSampl.Name .categoryId) && c.id.Equals(lstSampl.id)).ToList();
                if (lstprod.Count == 0)
                {
                    isSuccess = true;
                    ZoomCartAlbumStore.LstAlbumImgDetails.Add(imgDetails);
                    ZoomCartAlbumStore.AlbumCnt += 1;
                    myZoomAlbumHandler(this, null);

                }
                else
                {
                    MessageBox.Show("Already added the selected item to album. Please choose another one....", "Villart Photography", MessageBoxButton.OK, MessageBoxImage.Information);

                    this.Visibility = Visibility.Hidden;
                }
            }

            if (isSuccess)
            {
                //this.Close();
                System.Threading.Thread.Sleep(500);
                this.Visibility = Visibility.Hidden;
            }
            //this.Visibility = Visibility.Collapsed;
        }

    }
}
