﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for ZoomCart.xaml
    /// </summary>
    public partial class ZoomCart : UserControl
    {
        ListView listView;
        public List<Image> lstimg;
        public List<ImageDetails> lstImgDetails;
        ListViewItem listViewItem;
        public ImageDetails samp;
        public string currentPath;
        public Button btn;
        public event EventHandler myEventDecreaseCartIncreaseAlbumCntUpdate;
        public event EventHandler myEventDecreaseCartCnt;
        int i = 20;

        public ZoomCart()
        {
            InitializeComponent();
        }

        private void lstParent_TouchDown_1(object sender, TouchEventArgs e)
        {
            ItemsControl _sourceItemsControl = (ItemsControl)sender;
            var visual = e.OriginalSource as Visual;

            Window _topWindow = Window.GetWindow(_sourceItemsControl);
            Point _initialMousePosition = e.GetTouchPoint(_topWindow).Position;

            FrameworkElement _sourceItemContainer = _sourceItemsControl.ContainerFromElement(visual) as FrameworkElement;
            if (_sourceItemContainer != null)
            {
                //object _draggedData = _sourceItemContainer.DataContext;
                samp = _sourceItemContainer.DataContext as ImageDetails;
            }
        }

        Point enterPoint;

        private void lstParent_TouchUp_1(object sender, TouchEventArgs e)
        {
            //lstParent.ReleaseTouchCapture(e.TouchDevice);
            //e.Handled = true;
        }

        private void canvas1_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            TouchPoint tPnt = e.GetTouchPoint(canvas1);
            if (tPnt.Position.X < 150 && tPnt.Position.X > -70 && tPnt.Position.Y > -70 && tPnt.Position.Y < 70)
            {
                this.Cursor = Cursors.Hand;
                Close cls = e.Source as Close;
                cls.btnCart_TouchDown_1(sender, e);
            }
            else if (tPnt.Position.X < 150 && tPnt.Position.X > -75 && tPnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
                Close cls = e.Source as Close;
                cls.btnDelete_TouchDown_1(sender, e);
            }
        }

        private void canvas1_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            TouchPoint tPnt = e.GetTouchPoint(canvas1);
            if (tPnt.Position.X < -5 && tPnt.Position.X > -75 && tPnt.Position.Y > 0 && tPnt.Position.Y < 80)
            {
                this.Cursor = Cursors.Hand;
            }
            else if (tPnt.Position.X < -5 && tPnt.Position.X > -75 && tPnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
            }
            else
                this.Cursor = Cursors.Arrow;
        }

        private void lstParent_PreviewTouchMove_1(object sender, TouchEventArgs e)
        {
            TouchPoint tPnt = e.GetTouchPoint(canvas1);
            if (tPnt.Position.X >= 0 && tPnt.Position.X <= 950 && tPnt.Position.Y > 0)
            {
                this.Cursor = Cursors.Hand;
            }
            else if (tPnt.Position.X >= 0 && tPnt.Position.Y > (canvas1.ActualHeight - 80))
            {
                this.Cursor = Cursors.Hand;
            }
            else
                this.Cursor = Cursors.Arrow;
        }

        private void lstParent_PreviewTouchUp_1(object sender, TouchEventArgs e)
        {
            this.Cursor = Cursors.Hand;
            TouchPoint tPnt = e.GetTouchPoint(canvas1);
            if (tPnt.Position.X >= 0 && tPnt.Position.X <= 950 && tPnt.Position.Y > 0)
            {
                try
                {
                    MatrixTransform trsfrm = new MatrixTransform(0.3, 0, 0, 0.3, i + 10, 50);

                    Close cl = new Close();
                    cl.myCartEvent += cl_myCartEvent;
                    cl.myCloseEvent += cl_myCloseEvent;
                    cl.Name = "Close" + i;
                    cl.IsManipulationEnabled = true;
                    cl.samp = samp;
                    cl.RenderTransform = trsfrm;
                    canvas1.Children.Add(cl);
                    i = i + 20;
                    ZoomCartAlbumStore.LstZoomCartImgDetails.Remove(samp);
                    ZoomCartAlbumStore.CartCnt = ZoomCartAlbumStore.LstZoomCartImgDetails.Count;
                    myEventDecreaseCartCnt(this, null);
                }
                catch (Exception)
                {

                }
                lstParent.ItemsSource = ZoomCartAlbumStore.LstZoomCartImgDetails.ToList();
                lstParent.Items.Refresh();
                this.Cursor = Cursors.Arrow;
            }
            else
                this.Cursor = Cursors.Arrow;
        }

        List<string> lstthumbs;

        void canvas1_ManipulationDelta(object sender, ManipulationDeltaEventArgs args)
        {
            UIElement element = args.Source as UIElement;
            MatrixTransform xform = element.RenderTransform as MatrixTransform;
            Matrix matrix = xform.Matrix;

            ManipulationDelta delta = args.DeltaManipulation;
            Point center = args.ManipulationOrigin;
            matrix.Translate(-center.X, -center.Y);
            matrix.Scale(delta.Scale.X, delta.Scale.Y);
            matrix.Translate(center.X, center.Y);
            matrix.Translate(delta.Translation.X, delta.Translation.Y);
            xform.Matrix = matrix;

            args.Handled = true;
            base.OnManipulationDelta(args);
        }

        ManipulationModes currentMode = ManipulationModes.All;

        string str;
        void canvas1_ManipulationStarting(object sender, ManipulationStartingEventArgs args)
        {
            args.ManipulationContainer = canvas1;
            args.Mode = currentMode;

            // Adjust Z-order
            FrameworkElement element = args.Source as FrameworkElement;
            Panel pnl = element.Parent as Panel;

            for (int i = 0; i < pnl.Children.Count; i++)
                Panel.SetZIndex(pnl.Children[i],
                    pnl.Children[i] == element ? pnl.Children.Count : i);

            // Set Pivot
            Point center = new Point(element.ActualWidth / 2, element.ActualHeight / 2);
            str += "X: " + center.X + " Y: " + center.Y + "\n";
            center = element.TranslatePoint(center, this);
            args.Pivot = new ManipulationPivot(center, 48);

            args.Handled = true;

            base.OnManipulationStarting(args);
        }

        private void Canvas_DragOver(object sender, DragEventArgs e)
        {
            enterPoint = e.GetPosition(this.canvas1);
        }

        private void Canvas_DragEnter(object sender, DragEventArgs e)
        {
            if (!(e.Data.GetDataPresent("contact")) || (sender == e.Source))
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void Canvas_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("MyFormat"))
            {
                try
                {
                    MatrixTransform trsfrm = new MatrixTransform(0.3, 0, 0, 0.3, i + 10, 50);

                    Close cl = new Close();
                    cl.myCartEvent += cl_myCartEvent;
                    cl.myCloseEvent += cl_myCloseEvent;
                    cl.Name = "Close" + i;
                    cl.IsManipulationEnabled = true;
                    cl.samp = samp;
                    cl.RenderTransform = trsfrm;
                    canvas1.Children.Add(cl);
                    i = i + 20;
                    ZoomCartAlbumStore.LstZoomCartImgDetails.Remove(samp);
                    ZoomCartAlbumStore.CartCnt = ZoomCartAlbumStore.LstZoomCartImgDetails.Count;
                    myEventDecreaseCartCnt(this, null);
                }
                catch (Exception)
                {

                }
                lstParent.ItemsSource = ZoomCartAlbumStore.LstZoomCartImgDetails.ToList();
                lstParent.Items.Refresh();
            }
        }

        void cl_myCloseEvent(object sender, EventArgs e)
        {
            Close cl = sender as Close;
            canvas1.Children.Remove(cl);

            //StoreCompare_Cart.CmpreCnt = StoreCompare_Cart.lstCompare.Count;
            //myEventCmpreCntUpdate(this, null);
        }

        void cl_myCartEvent(object sender, EventArgs e)
        {
            bool isSuccess = false;
            Close cl = sender as Close;
            List<ImageDetails> imgDetails = cl.DataContext as List<ImageDetails>;
            

            foreach (ImageDetails lstSampl in imgDetails)
            {
                //List<ImageDetails> lstprod = ZoomCartAlbumStore.LstZoomCartImgDetails.Where(c => c.FileName.Equals(lstSampl.FileName)).ToList();
                List<ImageDetails> lstprod = ZoomCartAlbumStore.LstAlbumImgDetails.Where(c => c.FileName.Equals(lstSampl.FileName)).ToList();
                if (lstprod.Count == 0)
                {
                    isSuccess = true;
                    //ZoomCartAlbumStore.LstZoomCartImgDetails.Add(imgDetails);
                    if (imgDetails != null && imgDetails.Count > 0)
                           ZoomCartAlbumStore.LstAlbumImgDetails.Add(imgDetails[0]);
                    ZoomCartAlbumStore.AlbumCnt += 1;
                    myEventDecreaseCartIncreaseAlbumCntUpdate(this, null);
                    canvas1.Children.Remove(cl);
                }
                else
                {
                    MessageBox.Show("Already added the selected item to album. Please choose another one....", "Villart Photography", MessageBoxButton.OK, MessageBoxImage.Information);

                    canvas1.Children.Remove(cl);
                }
            }

            if (isSuccess)
            {
                //this.Close();
                System.Threading.Thread.Sleep(500);
                //this.Visibility = Visibility.Hidden;
            }



            //Close cl = sender as Close;
            //List<ImageDetails> imgDetails = cl.DataContext as List<ImageDetails>;
            //if (imgDetails != null && imgDetails.Count > 0)
            //    ZoomCartAlbumStore.LstAlbumImgDetails.Add(imgDetails[0]);

            //ZoomCartAlbumStore.AlbumCnt += 1;
            //myEventDecreaseCartIncreaseAlbumCntUpdate(this, null);

            //canvas1.Children.Remove(cl);
        }

        private void UserControl_Loaded_1(object sender, RoutedEventArgs e)
        {
           
            lstParent.DataContext = lstImgDetails;
            canvas1.ManipulationStarting += new EventHandler<ManipulationStartingEventArgs>(canvas1_ManipulationStarting);
            canvas1.ManipulationDelta += new EventHandler<ManipulationDeltaEventArgs>(canvas1_ManipulationDelta);
        }

        private void lstParent_ManipulationBoundaryFeedback_1(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
