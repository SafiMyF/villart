﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VillartTest
{
    /// <summary>
    /// Interaction logic for Category.xaml
    /// </summary>
    public partial class Category : UserControl
    {
        public Boolean b = new Boolean();
        Listing lst = new Listing();
        public Category()
        {
            InitializeComponent();
        }

        public delegate void myCategListing(object sender, EventArgs e);
        public event myCategListing myCategListHandler;

        public event myCategListing myCategListHandlerPortrait;
        public event myCategListing myCategListHandlerFashions;
        public event myCategListing myCategListHandlerKids;
        public event myCategListing myCategListHandlerCorporate;
        public event myCategListing myCategListHandlerOthers;

        private void btnWedding_Click(object sender, RoutedEventArgs e)
        {
            myCategListHandler(this, null);
        }

        private void btnWedding_TouchDown(object sender, TouchEventArgs e)
        {
            myCategListHandler(this, null);
        }

        private void btnPortrait_TouchDown(object sender, TouchEventArgs e)
        {
            myCategListHandlerPortrait(this, null);
        }

        private void btnKids_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCategListHandlerKids(this, null);
        }

        private void btnFashions_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCategListHandlerFashions(this, null);
        }

        private void Corporate_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCategListHandlerCorporate(this, null);
        }

        private void btnOthers_TouchDown_1(object sender, TouchEventArgs e)
        {
            myCategListHandlerOthers(this, null);
        }

        private void btnPortrait_Click_1(object sender, RoutedEventArgs e)
        {
            myCategListHandlerPortrait(this, null);
        }
    }
}
